#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pickle
import numpy
from music21 import instrument, note, stream, chord
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.layers import BatchNormalization as BatchNorm
from keras.layers import Activation
from datetime import datetime
import argparse
from network import create_network

class MusicGenerator:
    def generate(self, model_path, generated_file_path):
        with open('data/notes', 'rb') as filepath:
            notes = pickle.load(filepath)

        pitchnames = sorted(set(item for item in notes))
        n_vocab = len(set(notes))

        network_input, normalized_input = self.prepare_sequences(notes, pitchnames, n_vocab)
        model = create_network(normalized_input, n_vocab)
        model.load_weights(model_path)

        prediction_output = self.generate_notes(model, network_input, pitchnames, n_vocab)
        self.create_midi(prediction_output)

    def prepare_sequences(self, notes, pitchnames, n_vocab):
        note_to_int = dict((note, number) for number, note in enumerate(pitchnames))

        sequence_length = 100
        network_input = []
        output = []
        for i in range(0, len(notes) - sequence_length, 1):
            sequence_in = notes[i:i + sequence_length]
            sequence_out = notes[i + sequence_length]
            network_input.append([note_to_int[char] for char in sequence_in])
            output.append(note_to_int[sequence_out])

        n_patterns = len(network_input)

        normalized_input = numpy.reshape(network_input, (n_patterns, sequence_length, 1))
        normalized_input = normalized_input / float(n_vocab)

        return (network_input, normalized_input)


    def generate_notes(self, model, network_input, pitchnames, n_vocab):
        start = numpy.random.randint(0, len(network_input)-1)

        int_to_note = dict((number, note) for number, note in enumerate(pitchnames))

        pattern = network_input[start]
        prediction_output = []

        for note_index in range(500):
            prediction_input = numpy.reshape(pattern, (1, len(pattern), 1))
            prediction_input = prediction_input / float(n_vocab)

            prediction = model.predict(prediction_input, verbose=0)

            index = numpy.argmax(prediction)
            result = int_to_note[index]
            prediction_output.append(result)

            pattern.append(index)
            pattern = pattern[1:len(pattern)]

        return prediction_output

    def create_midi(self, prediction_output):
        offset = 0
        output_notes = []
        today = datetime.now()

        for pattern in prediction_output:
            if ('.' in pattern) or pattern.isdigit():
                notes_in_chord = pattern.split('.')
                notes = []
                for current_note in notes_in_chord:
                    new_note = note.Note(int(current_note))
                    new_note.storedInstrument = instrument.Piano()
                    notes.append(new_note)
                new_chord = chord.Chord(notes)
                new_chord.offset = offset
                output_notes.append(new_chord)
            else:
                new_note = note.Note(pattern)
                new_note.offset = offset
                new_note.storedInstrument = instrument.Piano()
                output_notes.append(new_note)

            offset += 0.5

        midi_stream = stream.Stream(output_notes)

        midi_stream.write('midi', fp='generated-at-' + today.strftime('%d.%m.%Y-%H_%M_%S') + '.mid')

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    today = datetime.today()

    ap.add_argument('-m', '--model_path', required=False, help='Model name and file path', default='weights.hdf5')
    ap.add_argument('-g', '--generated_file_path', required=False, help='File name and path to the generated file', default='generated_at_' + today.strftime('%d.%m.%Y-%H_%M_%S')  + '.hdf5')

    args = vars(ap.parse_args())

    music_generator = MusicGenerator()
    music_generator.generate(args['model_path'], args['generated_file_path'])
