#!/usr/bin/python3

import glob
import pickle
import numpy
from music21 import converter, instrument, note, chord
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.layers import Activation
from keras.layers import BatchNormalization as BatchNorm
from keras.utils import np_utils
from keras.callbacks import ModelCheckpoint
import argparse
from network import create_network
from datetime import datetime

class NetworkTrainer:
    def train_network(self, file_path):
        notes = self.get_notes()

        n_vocab = len(set(notes))

        network_input, network_output = self.prepare_sequences(notes, n_vocab)

        model = create_network(network_input, n_vocab)

        self.train(model, file_path, network_input, network_output)

    def get_notes(self):
        notes = []

        for file in glob.glob("midi_songs/*.mid"):
            try:
                midi = converter.parse(file)
                print("Parsing %s" % file)

                notes_to_parse = None
                s2 = instrument.partitionByInstrument(midi)
                notes_to_parse = s2.parts[0].recurse()
                
                for element in notes_to_parse:
                    if isinstance(element, note.Note):
                        notes.append(str(element.pitch))
                    elif isinstance(element, chord.Chord):
                        notes.append('.'.join(str(n) for n in element.normalOrder))
            except:
                notes_to_parse = midi.flat.notes

        with open('data/notes', 'wb') as filepath:
            pickle.dump(notes, filepath)

        return notes

    def prepare_sequences(self, notes, n_vocab):
        sequence_length = 100

        pitchnames = sorted(set(item for item in notes))

        note_to_int = dict((note, number) for number, note in enumerate(pitchnames))

        network_input = []
        network_output = []

        for i in range(0, len(notes) - sequence_length, 1):
            sequence_in = notes[i:i + sequence_length]
            sequence_out = notes[i + sequence_length]
            network_input.append([note_to_int[char] for char in sequence_in])
            network_output.append(note_to_int[sequence_out])

        n_patterns = len(network_input)

        network_input = numpy.reshape(network_input, (n_patterns, sequence_length, 1))
        network_input = network_input / float(n_vocab)

        network_output = np_utils.to_categorical(network_output)

        return (network_input, network_output)

    def train(self, model, file_path, network_input, network_output):
        checkpoint = ModelCheckpoint(
            file_path,
            monitor='loss',
            verbose=0,
            save_best_only=True,
            mode='min'
        )
        callbacks_list = [checkpoint]

        model.fit(network_input, network_output, epochs=200, batch_size=128, callbacks=callbacks_list)

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    today = datetime.today()

    ap.add_argument('-f', '--file_path', required=False, help='Model name and file path', default='improved_weights_.hdf5')
    
    args = vars(ap.parse_args())

    network_trainer = NetworkTrainer()
    network_trainer.train_network(args['file_path'])